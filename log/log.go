package log

import (
	"github.com/sirupsen/logrus"
)

const (
	PanicLevel = logrus.PanicLevel
	FatalLevel = logrus.FatalLevel
	ErrorLevel = logrus.ErrorLevel
	WarnLevel  = logrus.WarnLevel
	InfoLevel  = logrus.InfoLevel
	DebugLevel = logrus.DebugLevel
	TraceLevel = logrus.TraceLevel
)

var AllLevels = []Level{
	PanicLevel,
	FatalLevel,
	ErrorLevel,
	WarnLevel,
	InfoLevel,
	DebugLevel,
	TraceLevel,
}

type (
	Logger interface {
		logrus.Ext1FieldLogger
		AddHook(hook Hook)
		SetLevel(level Level)
		SetUp() error
		TearDown() error
	}
	Hook  = logrus.Hook
	Level = logrus.Level
	Entry = logrus.Entry
)

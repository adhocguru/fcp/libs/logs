package tracing

import (
	"context"
	"fmt"
	"strings"

	"github.com/grpc-ecosystem/grpc-opentracing/go/otgrpc"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/opentracing/opentracing-go/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/stats"
)

const EventKey = "event"

type GrpcClientInterceptor = grpc.UnaryClientInterceptor
type GrpcServerInterceptor = grpc.UnaryServerInterceptor

var GRPCComponentTag = opentracing.Tag{
	Key:   string(ext.Component),
	Value: "gRPC",
}

type (
	GrpcServerStatsHandler interface {
		stats.Handler
	}
	GrpcClientStatsHandler interface {
		stats.Handler
	}
)

type statsHandler struct {
	tracer   Tracer
	wrapSpan bool
	server   bool
}

func NewGrpcServerStatsHandler(tracer Tracer, wrapSpan bool) GrpcServerStatsHandler {
	return &statsHandler{
		tracer:   tracer,
		wrapSpan: wrapSpan,
		server:   true,
	}
}

func NewGrpcClientStatsHandler(tracer Tracer, wrapSpan bool) GrpcClientStatsHandler {
	return &statsHandler{
		tracer:   tracer,
		wrapSpan: wrapSpan,
	}
}

func NewClientInterceptor(tracer opentracing.Tracer, logPayloads bool, ignorePayloadMethods ...string) GrpcClientInterceptor {
	return func(
		ctx context.Context,
		method string,
		req, resp interface{},
		cc *grpc.ClientConn,
		invoker grpc.UnaryInvoker,
		opts ...grpc.CallOption,
	) error {
		var options []otgrpc.Option
		var trailer metadata.MD
		otgrpc.SpanDecorator(
			func(span opentracing.Span, method string, req, resp interface{}, grpcError error) {
				fillSpanWithMeta(span, trailer)
			},
		)
		if logPayloads {
			var methodIgnored bool
			for _, ignoredMethod := range ignorePayloadMethods {
				if method == ignoredMethod {
					methodIgnored = true
				}
			}
			if !methodIgnored {
				options = append(options, otgrpc.LogPayloads())
			}
		}
		interceptor := otgrpc.OpenTracingClientInterceptor(tracer, options...)
		opts = append(opts, grpc.Trailer(&trailer))

		return interceptor(ctx, method, req, resp, cc, invoker, opts...)
	}
}

func NewServerInterceptor(tracer opentracing.Tracer, logPayloads bool, ignorePayloadMethods ...string) GrpcServerInterceptor {
	interceptorLogPayloads := otgrpc.OpenTracingServerInterceptor(tracer, otgrpc.LogPayloads())
	interceptor := otgrpc.OpenTracingServerInterceptor(tracer)

	if !logPayloads {
		return interceptor
	}

	newInterceptor := func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		for _, ignoredMethod := range ignorePayloadMethods {
			if strings.Contains(info.FullMethod, ignoredMethod) {
				return interceptor(ctx, req, info, handler)
			}
		}
		return interceptorLogPayloads(ctx, req, info, handler)
	}

	return newInterceptor
}

func fillSpanWithMeta(span opentracing.Span, meta metadata.MD) {
	if meta != nil && len(meta) > 0 {
		for key, message := range meta {
			for _, messageValue := range message {
				span.LogFields(
					log.String(key, messageValue),
				)
			}
		}
	}
}

func (h *statsHandler) TagRPC(ctx context.Context, tagInfo *stats.RPCTagInfo) context.Context {
	if !h.wrapSpan {
		return ctx
	}

	spanCtx := h.extractSpanContext(h.tracer, ctx)
	span := h.tracer.StartSpan("TRACE: "+tagInfo.FullMethodName, opentracing.FollowsFrom(spanCtx), GRPCComponentTag)
	newCtx, _ := h.injectSpanToMetadata(h.tracer, span, ctx)

	return opentracing.ContextWithSpan(newCtx, span)
}

func (h *statsHandler) HandleRPC(ctx context.Context, s stats.RPCStats) {
	span := opentracing.SpanFromContext(ctx)
	if span == nil {
		return
	}

	switch st := s.(type) {
	case *stats.Begin:
		span.LogFields(log.String(EventKey, "RPC started"))
	case *stats.InPayload:
		span.LogFields(log.String(EventKey, fmt.Sprintf("Payload received: length=%d", st.Length)))
	case *stats.InHeader:
		span.LogFields(log.String(EventKey, "Header received"))
	case *stats.InTrailer:
		span.LogFields(log.String(EventKey, "Trailer received"))
	case *stats.OutPayload:
		span.LogFields(log.String(EventKey, "Payload sent"))
	case *stats.OutHeader:
		span.LogFields(log.String(EventKey, "Header sent"))
	case *stats.OutTrailer:
		span.LogFields(log.String(EventKey, "Trailer sent"))
	case *stats.End:
		span.LogFields(log.String(EventKey, "RPC ended"))
		if h.wrapSpan {
			span.Finish()
		}
	}
}

func (h *statsHandler) TagConn(ctx context.Context, tagInfo *stats.ConnTagInfo) context.Context {
	return ctx
}

func (h *statsHandler) HandleConn(ctx context.Context, status stats.ConnStats) {}

func (h *statsHandler) extractSpanContext(tracer opentracing.Tracer, ctx context.Context) opentracing.SpanContext {
	var sc opentracing.SpanContext
	sc = h.spanContextFromContext(ctx)
	if sc != nil {
		return sc
	}

	sc = h.extractSpanContextFromMetadata(tracer, ctx)
	return sc
}

func (h *statsHandler) spanContextFromContext(ctx context.Context) opentracing.SpanContext {
	if parentSpan := opentracing.SpanFromContext(ctx); parentSpan != nil {
		return parentSpan.Context()
	}
	return nil
}

func (h *statsHandler) injectSpanToMetadata(tracer opentracing.Tracer, span opentracing.Span, ctx context.Context) (context.Context, error) {
	md := metadata.MD{}
	if h.server {
		if tmpMD, ok := metadata.FromIncomingContext(ctx); ok {
			md = tmpMD.Copy()
		}
	} else {
		if tmpMD, ok := metadata.FromOutgoingContext(ctx); ok {
			md = tmpMD.Copy()
		}
	}

	if err := tracer.Inject(span.Context(), opentracing.HTTPHeaders, NewMetadataReaderWriter(md)); err != nil {
		return ctx, err
	}

	if h.server {
		return metadata.NewIncomingContext(ctx, md), nil
	}

	return metadata.NewOutgoingContext(ctx, md), nil
}

func (h *statsHandler) extractSpanContextFromMetadata(tracer opentracing.Tracer, ctx context.Context) opentracing.SpanContext {
	md := metadata.MD{}
	if h.server {
		if tmpMD, ok := metadata.FromIncomingContext(ctx); ok {
			md = tmpMD
		}
	} else {
		if tmpMD, ok := metadata.FromOutgoingContext(ctx); ok {
			md = tmpMD
		}
	}

	spanContext, err := tracer.Extract(opentracing.HTTPHeaders, NewMetadataReaderWriter(md))
	if err != nil {
		return nil
	}

	return spanContext
}

type MetadataReaderWriter struct {
	md metadata.MD
}

// NewMetadataReaderWriter creates an object that implements the opentracing.TextMapReader and opentracing.TextMapWriter interfaces
func NewMetadataReaderWriter(md metadata.MD) *MetadataReaderWriter {
	return &MetadataReaderWriter{md: md}
}

func (mrw *MetadataReaderWriter) ForeachKey(handler func(string, string) error) error {
	for key, values := range mrw.md {
		for _, value := range values {
			if err := handler(key, value); err != nil {
				return err
			}
		}
	}
	return nil
}

func (mrw *MetadataReaderWriter) Set(key, value string) {
	// headers should be lowercase
	k := strings.ToLower(key)
	mrw.md[k] = append(mrw.md[k], value)
}

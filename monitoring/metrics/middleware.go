package metrics

import (
	"github.com/labstack/echo/middleware"
	"github.com/prometheus/client_golang/prometheus"
)

type (
	PrometheusConfig struct {
		Skipper   middleware.Skipper
		Namespace string
	}
)

var (
	echoReqQps      *prometheus.CounterVec
	echoReqDuration *prometheus.SummaryVec
)

func initCollector(namespace string) {
	echoReqQps = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "http_request_total",
			Help:      "HTTP requests processed.",
		},
		[]string{"code", "method", "host", "url"},
	)
	echoReqDuration = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace: namespace,
			Name:      "http_request_duration_seconds",
			Help:      "HTTP request latencies in seconds.",
		},
		[]string{"method", "host", "url"},
	)
	prometheus.MustRegister(echoReqQps, echoReqDuration)
}

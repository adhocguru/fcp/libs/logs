package checkers

import (
	"github.com/dimiro1/health"
)

type ShutDownChecker struct{}

func ShutDown() *ShutDownChecker {
	return &ShutDownChecker{}
}

func (ShutDownChecker) Check() health.Health {
	check := health.NewHealth()
	check.Down()

	return check
}
